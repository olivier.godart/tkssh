#!/usr/bin/env python3

from tkinter import *
from tkinter import messagebox
from tkinter.ttk import *
import os
import sshconfig

#Command to start a new ssh session (replace hostname with {0}):
openhost_command='gnome-terminal --title="{0}" -- ssh {0}'
icon_filename='tkssh.png'

def openhost(event):
    sel=hostlist.curselection()
    if sel:
        host=hostlist.get(sel[0])
        print ("Opening",host)
        os.system(openhost_command.format(host))

def addhost():
    def save():
        name=nameinput.get()
        if not name:
            messagebox.showerror("Missing value","Name is mandatory")
            return
        sshconfig.addhost(name,nethostnameinput.get(),userinput.get(),portinput.get())
        hostlist.delete(0,END)
        makeHostList()
        editwin.destroy()

    editwin = Toplevel(window)
    editwin.title("New host")
    editframe = Frame(editwin)
    editframe.pack(padx=10, pady=10)

    namelabel = Label(editframe, text="Name: ")
    namelabel.grid(row=0,column=0, padx=2, pady=2)
    nameinput = Entry(editframe)
    nameinput.grid(row=0,column=1, padx=2, pady=2)

    nethostnamelabel = Label(editframe, text="Network hostname: ")
    nethostnamelabel.grid(row=1,column=0, padx=2, pady=2)
    nethostnameinput = Entry(editframe)
    nethostnameinput.grid(row=1,column=1, padx=2, pady=2)

    userlabel = Label(editframe, text="User: ")
    userlabel.grid(row=2,column=0, padx=2, pady=2)
    userinput = Entry(editframe)
    userinput.grid(row=2,column=1, padx=2, pady=2)

    portlabel = Label(editframe, text="Port: ")
    portlabel.grid(row=3,column=0, padx=2, pady=2)
    portinput = Entry(editframe)
    portinput.grid(row=3,column=1, padx=2, pady=2)

    buttonsframe = Frame(editwin)
    buttonsframe.pack(side=BOTTOM, padx=10, pady=10)
    savebutton = Button(buttonsframe, text ="Save", command=save)
    savebutton.grid(row=0,column=1, padx=5, pady=5)
    cancelbutton = Button(buttonsframe, text ="Cancel", command=editwin.destroy)
    cancelbutton.grid(row=0,column=0, padx=5, pady=5)

def edithost():
    try:
        oldname=hostlist.get(hostlist.curselection()[0])
    except:
        messagebox.showerror("No host selected","Please select a host to edit")
        return

    def save():
        name=nameinput.get()
        if not name:
            messagebox.showerror("Missing value","Name is mandatory")
            return
        sshconfig.changehost(oldname,name,nethostnameinput.get(),userinput.get(),portinput.get())
        hostlist.delete(0,END)
        makeHostList()
        editwin.destroy()

    editwin = Toplevel(window)
    editwin.title("Edit host")
    editframe = Frame(editwin)
    editframe.pack(padx=10, pady=10)

    namelabel = Label(editframe, text="Name: ")
    namelabel.grid(row=0,column=0, padx=2, pady=2)
    nameinput = Entry(editframe)
    nameinput.grid(row=0,column=1, padx=2, pady=2)
    nameinput.insert(0, oldname)

    nethostnamelabel = Label(editframe, text="Network hostname: ")
    nethostnamelabel.grid(row=1,column=0, padx=2, pady=2)
    nethostnameinput = Entry(editframe)
    nethostnameinput.grid(row=1,column=1, padx=2, pady=2)
    try:
        nethostnameinput.insert(0, sshconfig.hosts[oldname].hostname)
    except:
        pass

    userlabel = Label(editframe, text="User: ")
    userlabel.grid(row=2,column=0, padx=2, pady=2)
    userinput = Entry(editframe)
    userinput.grid(row=2,column=1, padx=2, pady=2)
    try:
        userinput.insert(0, sshconfig.hosts[oldname].user)
    except:
        pass

    portlabel = Label(editframe, text="Port: ")
    portlabel.grid(row=3,column=0, padx=2, pady=2)
    portinput = Entry(editframe)
    portinput.grid(row=3,column=1, padx=2, pady=2)
    try:
        portinput.insert(0, sshconfig.hosts[oldname].port)
    except:
        pass

    buttonsframe = Frame(editwin)
    buttonsframe.pack(side=BOTTOM, padx=10, pady=10)
    savebutton = Button(buttonsframe, text ="Save", command=save)
    savebutton.grid(row=0,column=1, padx=5, pady=5)
    cancelbutton = Button(buttonsframe, text ="Cancel", command=editwin.destroy)
    cancelbutton.grid(row=0,column=0, padx=5, pady=5)

def duphost():
    try:
        oldname=hostlist.get(hostlist.curselection()[0])
    except:
        messagebox.showerror("No host selected","Please select a host to duplicate")
        return

    def save():
        name=nameinput.get()
        if not name:
            messagebox.showerror("Missing value","Name is mandatory")
            return
        if name == oldname:
            messagebox.showerror("Name must be unique","Please change the name before saving")
            return
        sshconfig.addhost(name,nethostnameinput.get(),userinput.get(),portinput.get())
        hostlist.delete(0,END)
        makeHostList()
        editwin.destroy()

    editwin = Toplevel(window)
    editwin.title("Duplicate host")
    editframe = Frame(editwin)
    editframe.pack(padx=10, pady=10)

    namelabel = Label(editframe, text="Name: ")
    namelabel.grid(row=0,column=0, padx=2, pady=2)
    nameinput = Entry(editframe)
    nameinput.grid(row=0,column=1, padx=2, pady=2)
    nameinput.insert(0, oldname)

    nethostnamelabel = Label(editframe, text="Network hostname: ")
    nethostnamelabel.grid(row=1,column=0, padx=2, pady=2)
    nethostnameinput = Entry(editframe)
    nethostnameinput.grid(row=1,column=1, padx=2, pady=2)
    try:
        nethostnameinput.insert(0, sshconfig.hosts[oldname].hostname)
    except:
        pass

    userlabel = Label(editframe, text="User: ")
    userlabel.grid(row=2,column=0, padx=2, pady=2)
    userinput = Entry(editframe)
    userinput.grid(row=2,column=1, padx=2, pady=2)
    try:
        userinput.insert(0, sshconfig.hosts[oldname].user)
    except:
        pass

    portlabel = Label(editframe, text="Port: ")
    portlabel.grid(row=3,column=0, padx=2, pady=2)
    portinput = Entry(editframe)
    portinput.grid(row=3,column=1, padx=2, pady=2)
    try:
        portinput.insert(0, sshconfig.hosts[oldname].port)
    except:
        pass

    buttonsframe = Frame(editwin)
    buttonsframe.pack(side=BOTTOM, padx=10, pady=10)
    savebutton = Button(buttonsframe, text ="Save", command=save)
    savebutton.grid(row=0,column=1, padx=5, pady=5)
    cancelbutton = Button(buttonsframe, text ="Cancel", command=editwin.destroy)
    cancelbutton.grid(row=0,column=0, padx=5, pady=5)

def delhost():
    try:
        oldname=hostlist.get(hostlist.curselection()[0])
    except:
        messagebox.showerror("No host selected","Please select a host to delete")
        return

    def save():
        sshconfig.delhost(oldname)
        hostlist.delete(0,END)
        makeHostList()
        editwin.destroy()

    editwin = Toplevel(window)
    editwin.title("Delete host")
    editframe = Frame(editwin)
    editframe.pack(padx=10, pady=10)

    namelabel = Label(editframe, text="Name: ")
    namelabel.grid(row=0,column=0, padx=2, pady=2)

    delnamelabel = Label(editframe, text=oldname)
    delnamelabel.grid(row=0,column=1, padx=2, pady=2)

    buttonsframe = Frame(editwin)
    buttonsframe.pack(side=BOTTOM, padx=10, pady=10)
    savebutton = Button(buttonsframe, text ="Save", command=save)
    savebutton.grid(row=0,column=1, padx=5, pady=5)
    cancelbutton = Button(buttonsframe, text ="Cancel", command=editwin.destroy)
    cancelbutton.grid(row=0,column=0, padx=5, pady=5)

def makeHostList():
    hostnames = list(sshconfig.hosts)
    hostnames.sort()
    for h in hostnames:
        hostlist.insert(END,h)

def searchEntryPlaceholder(event=None):
    if not searchInput.get():
        searchInput.insert(0, 'Search...')
        searchInput.config(foreground = 'grey')
def searchEntryFocus(event=None):
    if searchInput.get() == 'Search...':
        searchInput.delete(0, END)
        searchInput.config(foreground = 'black')
def searchEntryChange(event=None):
    search=searchInput.get()
    hostlist.selection_clear(0, END)
    if search:
        for i, h in enumerate(hostlist.get(0, END)):
            if h.startswith(search):
                hostlist.selection_set(i)
                hostlist.see(i)
                break
def windowKeyPress(event):
    if window.focus_get() != searchInput:
        searchInput.focus()
        if event.char.isalnum():
            searchEntryFocus()
            searchInput.insert(END,event.char)

sshconfig.parse()

window = Tk(className='tkssh')
window.bind('<KeyPress>', windowKeyPress)
window.bind('<Return>', openhost)
frame = Frame(window)
frame.pack(padx=10, pady=10, fill=BOTH, expand=1)

searchInput = Entry(frame)
searchEntryPlaceholder()
searchInput.bind('<FocusIn>', searchEntryFocus)
searchInput.bind('<FocusOut>', searchEntryPlaceholder)
searchInput.bind('<KeyRelease>', searchEntryChange)
searchInput.pack(pady=5, side=TOP)

scrollbar = Scrollbar(frame, orient=VERTICAL)
hostlist = Listbox(frame, yscrollcommand=scrollbar.set)
scrollbar.config(command=hostlist.yview)
scrollbar.pack(side=RIGHT, fill=Y)
makeHostList()
hostlist.bind("<Double-Button-1>", openhost)
hostlist.pack(side=LEFT, fill=BOTH, expand=1)

menubar = Menu(window)
menu1 = Menu(menubar, tearoff=0)
menu1.add_command(label="Quit", command=window.quit)
menubar.add_cascade(label="File", menu=menu1)
menu2 = Menu(menubar, tearoff=0)
menu2.add_command(label="New host...", command=addhost)
menu2.add_command(label="Duplicate host...", command=duphost)
menu2.add_command(label="Edit host...", command=edithost)
menu2.add_command(label="Delete host...", command=delhost)
menubar.add_cascade(label="Edit", menu=menu2)
window.config(menu=menubar)
window.title("tkssh")
window.iconphoto(True, PhotoImage(file=os.path.join(os.path.dirname(os.path.realpath(__file__)),icon_filename)))


window.mainloop()
#window.destroy()
