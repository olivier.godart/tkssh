# tkssh

A simple GUI for launching ssh sessions and managing your .ssh/config hosts.

I thought such a tool was missing. The goal is not to reinvent the wheel, but
use existing standard tools as much as possible (openssh, gnome-terminal).

Written in python and tkinter.

Feel free to improve this program. Cool improvements would be for example:
*  session logs
*  groups (using #tkssh-group keyword in the ssh config file)
*  supporting other environments than gnome (other terminals, with auto-detection or a config file)
*  packaging (deb,rpm,...), standard directories, .desktop file installer...

## Installation
1. Copy the folder tkssh to /usr/local/lib/ or to ~/.local/lib/
2. Edit the file tkssh.desktop to set the correct paths for the Exec and Icon properties
2. Copy the file tkssh.desktop to /usr/local/share/applications/ or ~/.local/share/applications/
