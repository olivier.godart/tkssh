#!/usr/bin/env python3

import os

config_filename="~/.ssh/config"
hosts=dict()
groups=dict()

class Host:
    def __init__(self, name):
        self.name = name
        self.hidden = False
        

def parse():
    global hosts, groups
    hosts.clear()
    groups.clear()
    with open(os.path.expanduser(config_filename)) as f:
        configlines=f.readlines()
    for line in configlines:
        if line.startswith("Host"):
            hostName=line.split('#',1)[0].split(None,1)[1].strip()
            if '*' not in hostName:
                currentHost=Host(hostName)
                hosts[hostName]=currentHost
            else: # don't read defaults
                break
        elif "hostname" in line.split('#',1)[0].strip(): #TODO use regex
            currentHost.hostname=line.split('#',1)[0].split("hostname",1)[1].strip()
        elif "user" in line.split('#',1)[0].strip(): #TODO use regex
            currentHost.user=line.split('#',1)[0].split("user",1)[1].strip()
        elif "port" in line.split('#',1)[0].strip(): #TODO use regex
            currentHost.port=line.split('#',1)[0].split("port",1)[1].strip()
        if "#tkssh-hidden" in line:
            currentHost.hidden = True
        if "#tkssh-group" in line:
            group = line.split('#tkssh-group',1)[1].strip()
            if group in groups:
                groups[group].append(currentHost.name)
            else:
                groups[group]=[currentHost.name]
        

## add the new host after all other hosts, but before any host containing '*'
def addhost(name,hostname,user,port):
    global hosts, groups
    with open(os.path.expanduser(config_filename)) as f:
        configlines=f.readlines()
    for i,line in enumerate(configlines):
        if line.startswith("Host") and '*' in line:
            #insert here then break
            configlines.insert(i,"Host "+name+"\n")
            i+=1
            if hostname:
                configlines.insert(i,"    hostname "+hostname+"\n")
                i+=1
            else:
                configlines.insert(i,"    hostname "+name+"\n")
                i+=1
            if user:
                configlines.insert(i,"    user "+user+"\n")
                i+=1
            if port:
                configlines.insert(i,"    port "+port+"\n")
                i+=1
            break
    else:
        #append here
        configlines.append("Host "+name)
        if hostname:
            configlines.append("    hostname "+hostname+"\n")
        else:
            configlines.append("    hostname "+name+"\n")
        if user:
            configlines.append("    user "+user+"\n")
        if port:
            configlines.append("    port "+port+"\n")
    with open(os.path.expanduser(config_filename), 'w') as f:
        f.writelines( configlines )
    #add the new hosts to hosts
    parse()

## change the host
def changehost(oldname,name,hostname,user,port):
    global hosts, groups
    with open(os.path.expanduser(config_filename)) as f:
        configlines=f.readlines()
    for i,line in enumerate(configlines):
        if line.split('#',1)[0].strip() == "Host "+oldname: #TODO use regex (to support any whitespaces)
            #replace host then break
            if len(line.split('#',1))>1:
                name+=' #'+line.split('#',1)[1]
            configlines[i]="Host "+name+"\n"
            i+=1
            while i<len(configlines) and not configlines[i].startswith("Host"):
                if hostname and "hostname" in configlines[i].split('#',1)[0]: #TODO use regex
                    if len(configlines[i].split('#',1))>1:
                        hostname+=' #'+configlines[i].split('#',1)[1]
                    configlines[i]="    hostname "+hostname+"\n"
                    hostname=None
                elif user and "user" in configlines[i].split('#',1)[0]: #TODO use regex
                    if len(configlines[i].split('#',1))>1:
                        user+=' #'+configlines[i].split('#',1)[1]
                    configlines[i]="    user "+user+"\n"
                    user=None
                elif port and "port" in configlines[i].split('#',1)[0]: #TODO use regex
                    if len(configlines[i].split('#',1))>1:
                        port+=' #'+configlines[i].split('#',1)[1]
                    configlines[i]="    port "+port+"\n"
                    port=None
                i+=1
            if hostname:
                configlines.insert(i,"    hostname "+hostname+"\n")
                i+=1
            if user:
                configlines.insert(i,"    user "+user+"\n")
                i+=1
            if port:
                configlines.insert(i,"    port "+port+"\n")
                i+=1
            break
    else:
        #not found
        return
    with open(os.path.expanduser(config_filename), 'w') as f:
        f.writelines( configlines )
    #refresh hosts
    parse()

## delete the host
def delhost(name):
    global hosts, groups
    with open(os.path.expanduser(config_filename)) as f:
        configlines=f.readlines()
    deletelines=[]
    for i,line in enumerate(configlines):
        if line.split('#',1)[0].strip() == "Host "+name: #TODO use regex (to support any whitespaces)
            #delete host then break
            deletelines.insert(0,i)
            i+=1
            while i<len(configlines) and not configlines[i].startswith("Host"):
                if ("hostname" in configlines[i].split('#',1)[0] #TODO use regex
                    or "user" in configlines[i].split('#',1)[0] #TODO use regex
                    or "port" in configlines[i].split('#',1)[0] #TODO use regex
                    or "#tkssh-hidden" in configlines[i]
                    or "#tkssh-group" in configlines[i]):
                        deletelines.insert(0,i)
                i+=1
            break
    else:
        #not found
        return
    for i in deletelines:
        del configlines[i]
    with open(os.path.expanduser(config_filename), 'w') as f:
        f.writelines( configlines )
    #refresh hosts
    parse()
